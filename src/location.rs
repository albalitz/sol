use std::fmt;

#[cfg(test)]
use mockito;
use reqwest::Response;
use serde::Deserialize;

#[derive(Debug)]
pub struct Location {
    pub lat: String,
    pub lon: String,
    pub utc_offset: Option<String>,
}
impl Location {
    pub fn from_coordinates(lat: &str, lon: &str) -> Self {
        Self {
            lat: lat.to_string(),
            lon: lon.to_string(),
            utc_offset: None,
        }
    }

    pub fn from_ip() -> Result<Self, LocationError> {
        let ip_information = match Location::request_ip_information() {
            Ok(r) => r,
            Err(e) => return Err(e),
        };
        let lat: String = match ip_information.latitude {
            Some(l) => l.to_string(),
            None => return Err(LocationError::InformationMissing),
        };
        let lon: String = match ip_information.longitude {
            Some(l) => l.to_string(),
            None => return Err(LocationError::InformationMissing),
        };
        let utc_offset = ip_information.utc_offset;
        Ok(Self {
            lat,
            lon,
            utc_offset,
        })
    }

    fn request_ip_information() -> Result<IPAPIResponse, LocationError> {
        let url = api_url();
        let request = reqwest::Client::new().get(url.as_str());
        let mut response: Response = match request.send() {
            Ok(r) => r,
            Err(_) => return Err(LocationError::APIUnreachable),
        };
        let ipapi_response: IPAPIResponse = match response.json() {
            Ok(r) => r,
            Err(_) => return Err(LocationError::UnexpectedJSON),
        };
        if let Some(_) = ipapi_response.error {
            match ipapi_response.reason {
                Some(s) => match s.as_str() {
                    "RateLimited" => return Err(LocationError::QuotaExceeded),
                    _ => return Err(LocationError::UnknownError),
                },
                None => {}
            }
        }
        Ok(ipapi_response)
    }
}
impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}, {}", self.lat, self.lon)
    }
}

#[derive(Deserialize, Debug)]
struct IPAPIResponse {
    error: Option<bool>,
    reason: Option<String>,
    latitude: Option<f64>,
    longitude: Option<f64>,
    utc_offset: Option<String>,
}

// TODO: error messages from https://ipapi.co/api/
#[derive(Debug)]
pub enum LocationError {
    APIUnreachable,
    UnexpectedJSON,
    InformationMissing,
    QuotaExceeded,
    UnknownError,
}
impl fmt::Display for LocationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg: &str = match self {
            LocationError::APIUnreachable => "can't reach API",
            LocationError::UnexpectedJSON => {
                "the JSON returned from the API doesn't match the expected format"
            }
            LocationError::InformationMissing => "information required to determine the location is missing from the API's JSON response",
            LocationError::QuotaExceeded => "rate limited",
            LocationError::UnknownError => "unknown error",
        };
        write!(f, "{}", msg)
    }
}

fn api_url() -> String {
    #[cfg(not(test))]
    let url: String = "https://ipapi.co/json".to_string();
    #[cfg(test)]
    let url: String = format!("{}/json", mockito::server_url());

    url
}

#[cfg(test)]
mod test_location_from_ip {
    use mockito::mock;

    use super::*;

    #[test]
    fn should_successfully_parse_valid_ipapi_json_to_location() {
        let _m = mock("GET", "/json")
            .with_body(
                r#"{
    "latitude": 59.3307,
    "longitude": 18.0718,
    "utc_offset": "+0200"
}"#,
            )
            .with_header("content-type", "application-json")
            .with_status(200)
            .create();
        let location: Location = Location::from_ip().unwrap();

        assert_eq!(location.lat, "59.3307".to_string());
        assert_eq!(location.lon, "18.0718".to_string());
        assert_eq!(location.utc_offset, Some("+0200".to_string()));
    }

    #[test]
    fn should_gracefully_handle_error_response() {
        let _m = mock("GET", "/json")
            .with_body(
                r#"{
    "error": true,
    "reason": "RateLimited"
}"#,
            )
            .with_header("content-type", "application-json")
            .with_status(200)
            .create();
        let result = Location::from_ip().err().unwrap();

        match result {
            LocationError::QuotaExceeded => {}
            _ => panic!(
                "Should be {:?}, got {:?}",
                LocationError::QuotaExceeded,
                result
            ),
        };
    }

    #[test]
    fn should_gracefully_handle_invalid_json() {
        let _m = mock("GET", "/json")
            .with_body("this is not json")
            .with_header("content-type", "application-json")
            .with_status(200)
            .create();
        let result = Location::from_ip().err().unwrap();

        match result {
            LocationError::UnexpectedJSON => {}
            _ => panic!(
                "Should be {:?}, got {:?}",
                LocationError::UnexpectedJSON,
                result
            ),
        };
    }

    #[test]
    fn should_gracefully_handle_missing_information() {
        let _m = mock("GET", "/json")
            .with_body(
                r#"{
    "lat": 42.4242
}"#,
            )
            .with_header("content-type", "application-json")
            .with_status(200)
            .create();
        let result = Location::from_ip().err().unwrap();

        match result {
            LocationError::InformationMissing => {}
            _ => panic!(
                "Should be {:?}, got {:?}",
                LocationError::InformationMissing,
                result
            ),
        };
    }
}
