use chrono::Duration;

pub fn format_duration(duration: Duration) -> String {
    let h = duration.num_hours();
    let m = duration.num_minutes() % 60;
    let s = duration.num_seconds()
        - (duration.num_hours() * 60 * 60)
        - (duration.num_minutes() % 60 * 60);
    format!("{:0>2}:{:0>2}:{:0>2}", h, m, s)
}

#[cfg(test)]
mod test_format_duration {
    use super::*;

    #[test]
    fn should_format_duration_as_hh_mm_ss_with_leading_zeroes() {
        assert_eq!(&format_duration(Duration::seconds(40344)), "11:12:24");
        assert_eq!(&format_duration(Duration::seconds(35000)), "09:43:20");
        assert_eq!(&format_duration(Duration::seconds(42424)), "11:47:04");
        assert_eq!(&format_duration(Duration::seconds(13337)), "03:42:17");
        assert_eq!(&format_duration(Duration::seconds(21966)), "06:06:06");
    }
}
