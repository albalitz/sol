use std::fmt;

use chrono::{DateTime, Duration, FixedOffset};
#[cfg(test)]
use mockito;
use reqwest::Response;
use serde::Deserialize;

use crate::location::Location;
use crate::utils::format_duration;

pub struct Sun {
    sunrise: DateTime<FixedOffset>,
    sunset: DateTime<FixedOffset>,
    day_length: Duration,
}
impl Sun {
    pub fn from_location(location: &Location, date: &str) -> Result<Self, SunriseSunsetAPIError> {
        let sun_information: SunriseSunsetAPIResponse =
            match Sun::request_sun_information(&location.lat, &location.lon, date) {
                Ok(si) => si,
                Err(e) => return Err(e),
            };
        match sun_information.results {
            SunriseSunsetAPIResults::WithResults {
                sunrise,
                sunset,
                day_length,
            } => {
                let sunrise: DateTime<FixedOffset> = match DateTime::parse_from_rfc3339(&sunrise) {
                    Ok(s) => s,
                    Err(_) => return Err(SunriseSunsetAPIError::InvalidDateResponse),
                };
                let sunset: DateTime<FixedOffset> = match DateTime::parse_from_rfc3339(&sunset) {
                    Ok(s) => s,
                    Err(_) => return Err(SunriseSunsetAPIError::InvalidDateResponse),
                };
                let day_length: Duration = Duration::seconds(day_length.into());
                Ok(Self {
                    sunrise,
                    sunset,
                    day_length,
                })
            }
            SunriseSunsetAPIResults::WithoutResults(_) => Err(SunriseSunsetAPIError::UnknownError),
        }
    }

    fn request_sun_information(
        lat: &str,
        lon: &str,
        date: &str,
    ) -> Result<SunriseSunsetAPIResponse, SunriseSunsetAPIError> {
        let url = api_url();
        let request = reqwest::Client::new().get(url.as_str()).query(&[
            ("lat", lat),
            ("lng", lon),
            ("formatted", "0"),
            ("date", date),
        ]);
        let mut response: Response = match request.send() {
            Ok(r) => r,
            Err(_) => return Err(SunriseSunsetAPIError::APIUnreachable),
        };
        let api_response: SunriseSunsetAPIResponse = match response.json() {
            Ok(r) => r,
            Err(_) => return Err(SunriseSunsetAPIError::UnexpectedJSON),
        };
        match api_response.status.as_str() {
            "OK" => Ok(api_response),
            "UNKNOWN_ERROR" => Err(SunriseSunsetAPIError::UnknownError),
            "INVALID_DATE" => Err(SunriseSunsetAPIError::InvalidDate),
            "INVALID_REQUEST" => Err(SunriseSunsetAPIError::InvalidRequest),
            _ => Err(SunriseSunsetAPIError::UnknownError),
        }
    }
}
impl fmt::Display for Sun {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "sunrise: {}, sunset: {} (day length: {})",
            self.sunrise,
            self.sunset,
            format_duration(self.day_length)
        )
    }
}

#[derive(Deserialize, Debug)]
struct SunriseSunsetAPIResponse {
    status: String,
    results: SunriseSunsetAPIResults,
}
#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum SunriseSunsetAPIResults {
    WithResults {
        sunrise: String,
        sunset: String,
        day_length: u32,
    },
    WithoutResults(String),
}

#[derive(Debug)]
pub enum SunriseSunsetAPIError {
    APIUnreachable,
    InvalidRequest,
    InvalidDate,
    InvalidDateResponse,
    UnexpectedJSON,
    UnknownError,
}
impl fmt::Display for SunriseSunsetAPIError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg: &str = match self {
            SunriseSunsetAPIError::APIUnreachable => "can't reach API",
            SunriseSunsetAPIError::InvalidRequest => "invalid request",
            SunriseSunsetAPIError::InvalidDate => "invalid date. Please check your input",
            SunriseSunsetAPIError::InvalidDateResponse => {
                "the date returned from the API doesn't match the expected format"
            }
            SunriseSunsetAPIError::UnexpectedJSON => {
                "the JSON returned from the API doesn't match the expected format"
            }
            SunriseSunsetAPIError::UnknownError => "unknown error",
        };
        write!(f, "{}", msg)
    }
}

fn api_url() -> String {
    #[cfg(not(test))]
    let url: String = "https://api.sunrise-sunset.org/json".to_string();
    #[cfg(test)]
    let url: String = format!("{}/json", mockito::server_url());

    url
}

#[cfg(test)]
mod test_sun_from_location {
    use super::*;

    use mockito::{mock, Matcher};

    #[test]
    fn should_successfully_parse_valid_json_to_sun() {
        let _m = mock("GET", "/json")
            .match_query(Matcher::AllOf(vec![
                Matcher::UrlEncoded("lat".into(), "59.3307".into()),
                Matcher::UrlEncoded("lng".into(), "18.0718".into()),
                Matcher::UrlEncoded("formatted".into(), "0".into()),
                Matcher::UrlEncoded("date".into(), "today".into()),
            ]))
            .with_body(
                r#"{
    "results": {
        "sunrise": "2019-10-04T05:00:16+00:00",
        "sunset": "2019-10-04T16:12:28+00:00",
        "day_length": 40332
    },
    "status": "OK"
}"#,
            )
            .with_header("content-type", "application-json")
            .with_status(200)
            .create();
        let location = Location::from_coordinates("59.3307", "18.0718");
        let sun: Sun = Sun::from_location(&location, "today").unwrap();

        assert_eq!(
            sun.sunrise,
            DateTime::parse_from_rfc3339("2019-10-04T05:00:16+00:00").unwrap()
        );
        assert_eq!(
            sun.sunset,
            DateTime::parse_from_rfc3339("2019-10-04T16:12:28+00:00").unwrap()
        );
        assert_eq!(sun.day_length, Duration::seconds(40332));
    }

    #[test]
    fn should_gracefully_handle_error_response() {
        let _m = mock("GET", "/json")
            .match_query(Matcher::AllOf(vec![
                Matcher::UrlEncoded("lat".into(), "59.3307".into()),
                Matcher::UrlEncoded("lng".into(), "18.0718".into()),
                Matcher::UrlEncoded("formatted".into(), "0".into()),
                Matcher::UrlEncoded("date".into(), "today".into()),
            ]))
            .with_body(
                r#"{
    "results": "",
    "status": "UNKNOWN_ERROR"
}"#,
            )
            .with_header("content-type", "application-json")
            .with_status(200)
            .create();
        let location = Location::from_coordinates("59.3307", "18.0718");
        let result = Sun::from_location(&location, "today").err().unwrap();

        match result {
            SunriseSunsetAPIError::UnknownError => {}
            _ => panic!(
                "Should be {:?}, got {:?}",
                SunriseSunsetAPIError::UnknownError,
                result
            ),
        };
    }

    #[test]
    fn should_gracefully_handle_invalid_json() {
        let _m = mock("GET", "/json")
            .match_query(Matcher::AllOf(vec![
                Matcher::UrlEncoded("lat".into(), "59.3307".into()),
                Matcher::UrlEncoded("lng".into(), "18.0718".into()),
                Matcher::UrlEncoded("formatted".into(), "0".into()),
                Matcher::UrlEncoded("date".into(), "today".into()),
            ]))
            .with_body("this is not json")
            .with_header("content-type", "application-json")
            .with_status(200)
            .create();
        let location = Location::from_coordinates("59.3307", "18.0718");
        let result = Sun::from_location(&location, "today").err().unwrap();

        match result {
            SunriseSunsetAPIError::UnexpectedJSON => {}
            _ => panic!(
                "Should be {:?}, got {:?}",
                SunriseSunsetAPIError::UnexpectedJSON,
                result
            ),
        };
    }

    #[test]
    fn should_gracefully_handle_missing_information() {
        let _m = mock("GET", "/json")
            .match_query(Matcher::AllOf(vec![
                Matcher::UrlEncoded("lat".into(), "59.3307".into()),
                Matcher::UrlEncoded("lng".into(), "18.0718".into()),
                Matcher::UrlEncoded("formatted".into(), "0".into()),
                Matcher::UrlEncoded("date".into(), "today".into()),
            ]))
            .with_body(
                r#"{
    "results": {
        "sunrise": "2019-10-04T05:00:16+00:00"
    },
    "status": "OK"
}"#,
            )
            .with_header("content-type", "application-json")
            .with_status(200)
            .create();
        let location = Location::from_coordinates("59.3307", "18.0718");
        let result = Sun::from_location(&location, "today").err().unwrap();

        match result {
            SunriseSunsetAPIError::UnexpectedJSON => {}
            _ => panic!(
                "Should be {:?}, got {:?}",
                SunriseSunsetAPIError::UnexpectedJSON,
                result
            ),
        };
    }
}
