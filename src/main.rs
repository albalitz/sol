use std::process::exit;

use clap::{crate_name, crate_version};
use clap::{App, AppSettings, Arg};

mod location;
mod sunrise_sunset;
mod utils;

use location::Location;
use sunrise_sunset::Sun;

fn main() {
    let args = App::new(crate_name!())
        .version(crate_version!())
        .setting(AppSettings::AllowNegativeNumbers)
        .about("Check sunrise and sunset times based on your IP address.")
        .arg(
            Arg::with_name("latitude")
                .help("Optional latitude. Provide this to use given coordinates instead of checking your IP's location.")
                .required(false)
                .requires("longitude"),
        )
        .arg(
            Arg::with_name("longitude")
                .help("Optional longitude. Provide this to use given coordinates instead of checking your IP's location.")
                .required(false)
                .requires("latitude"),
        )
        .arg(
            Arg::with_name("date")
                .long("--date")
                .help("Date for which to lookup sunrise/sunset information.")
                .long_help("Date for which to lookup sunrise/sunset information.
Check https://sunrise-sunset.org/api for supported formats.")
                .required(false)
                .takes_value(true)
                .default_value("today")
        )
        .get_matches();
    let location: Location;
    if args.is_present("latitude") && args.is_present("longitude") {
        location = Location::from_coordinates(
            args.value_of("latitude").unwrap(),
            args.value_of("longitude").unwrap(),
        );
    } else {
        location = match Location::from_ip() {
            Ok(l) => l,
            Err(e) => {
                eprintln!("Error finding location: {}", e);
                exit(1);
            }
        };
    }
    let date = args.value_of("date").unwrap();
    let sun: Sun = match Sun::from_location(&location, &date) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Error finding sunrise and sunset data: {}", e);
            exit(1);
        }
    };
    println!("Solar information for {}: {}", location, sun);
    println!("Note: these dates are returned by the API in UTC. Please calculate your actual local time yourself.")
}
