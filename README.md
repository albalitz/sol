# Sól
> Sól (Old Norse "Sun") or Sunna is the Sun personified in Norse mythology.

\- https://en.wikipedia.org/wiki/S%C3%B3l_(Sun)

Sól is a tool to check sunrise and sunset times for your current location, based on your IP address.
To achieve that, Sól uses [ipapi.co](https://ipapi.co/api/) to find your IP's coordinates, and [sunrise-sunset.org](https://sunrise-sunset.org/api) to find that location's sunrise and sunset times.

## Roadmap
- Weather?
